package SpringProjekt.MainSpring;

import SpringProjekt.MainSpring.entity.User;
import SpringProjekt.MainSpring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MainSpringApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

}
