package SpringProjekt.MainSpring.security;

import SpringProjekt.MainSpring.DTO.UserDTO;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TokenService tokenService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        String jwtToken = jwtAuthenticationToken.getJwtToken();
        UserDTO userDTO;
        try {
            userDTO = tokenService.parseToken(jwtToken);
        } catch (JwtException e) {
            throw new BadCredentialsException("Cannot parse received token");
        }
        jwtAuthenticationToken = new JwtAuthenticationToken(new ArrayList<>(), userDTO, jwtToken);
        SecurityContextHolder.getContext().setAuthentication(jwtAuthenticationToken);
        return jwtAuthenticationToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(JwtAuthenticationToken.class);
    }
}
