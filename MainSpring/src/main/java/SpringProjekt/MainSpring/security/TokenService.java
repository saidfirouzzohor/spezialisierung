package SpringProjekt.MainSpring.security;

import SpringProjekt.MainSpring.DTO.UserDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TokenService {

    @Value("${jwt.token.secret}")
    private String jwtSecret;

    @Value("${jwt.token.prefix}")
    private String jwtPrefix;

    public String generateToken(UserDTO userDTO) {
        Claims claims = Jwts.claims()
                .setSubject(userDTO.getEmail())
                .setIssuedAt(new Date());
        claims.put("userId", userDTO.getUserId());
        claims.put("name", userDTO.getName());

        return jwtPrefix + " " + Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, jwtSecret).compact();
    }

    public UserDTO parseToken(String token) {
        if (token.trim().length() == 0) {
            throw new JwtException("Empty token");
        }
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token.replace(jwtPrefix, "").trim())
                .getBody();
        return new UserDTO()
                .setUserId(Integer.parseInt(claims.get("userId").toString()))
            .setEmail(claims.getSubject())
            .setName(claims.get("name").toString());
}

}