package SpringProjekt.MainSpring.security;

import SpringProjekt.MainSpring.DTO.UserDTO;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {
    private final UserDTO userDTO;
    private final String jwtToken;

    public JwtAuthenticationToken(Collection<? extends GrantedAuthority> authorities, UserDTO userDTO, String jwtToken) {
        super(authorities);
        this.userDTO = userDTO;
        this.jwtToken = jwtToken;
    }

    public JwtAuthenticationToken(String jwtToken) {
        this(null, null, jwtToken);
        setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return "N/A";
    }

    @Override
    public UserDTO getPrincipal() {
        return userDTO;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}