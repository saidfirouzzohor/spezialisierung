package SpringProjekt.MainSpring.DTO;

import com.sun.istack.NotNull;

public class LoginDTO {

    public String getPassword() {
        return password;
    }

    public LoginDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public LoginDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    @NotNull
    private String password;

    @NotNull
    private String email;


    public LoginDTO(String password, String email) {

        this.password = password;
        this.email = email;
    }

    public LoginDTO() {
    }
}
