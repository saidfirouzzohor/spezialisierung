package SpringProjekt.MainSpring.DTO;

import com.sun.istack.NotNull;

public class AuthorizationDTO {

    @NotNull
    private UserDTO user;

    @NotNull
    private String jwt;

    public AuthorizationDTO(@NotNull String jwt, @NotNull UserDTO user) {
        this.jwt = jwt;
        this.user = user;
    }

    public UserDTO getUser() {
        return user;
    }

    public AuthorizationDTO setUser(@NotNull UserDTO user) {
        this.user = user;
        return this;
    }

    public String getJwt() {
        return jwt;
    }

    public AuthorizationDTO setJwt(@NotNull String jwt) {
        this.jwt = jwt;
        return this;
    }
}
