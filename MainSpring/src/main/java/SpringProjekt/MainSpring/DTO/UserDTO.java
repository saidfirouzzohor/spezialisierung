package SpringProjekt.MainSpring.DTO;

import com.sun.istack.NotNull;

public class UserDTO {
    //hier id, username, email und role und ähnliches ohne passwort
    // mit diesen daten arbeitest du im FRONTEND


    @NotNull
    private int Id;

    private String name;
    private String email;

    public UserDTO() {
    }

    public UserDTO(@NotNull int Id, @NotNull String name, @NotNull String email) {
        this.Id = Id;
        this.name = name;
        this.email = email;
    }

    public int getUserId() {
        return Id;
    }

    public UserDTO setUserId(int userId) {
        this.Id = userId;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserDTO setEmail(String email) {
        this.email = email;
        return this;
    }
}

