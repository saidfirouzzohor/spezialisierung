package SpringProjekt.MainSpring.controller;

import SpringProjekt.MainSpring.DTO.AuthorizationDTO;
import SpringProjekt.MainSpring.DTO.LoginDTO;
import SpringProjekt.MainSpring.DTO.UserDTO;
import SpringProjekt.MainSpring.entity.User;
import SpringProjekt.MainSpring.repository.UserRepository;
import SpringProjekt.MainSpring.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    TokenService tokenService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void registerUser(@RequestBody User user) {
        String password = bCryptPasswordEncoder.encode(user.getPassword());
        userRepository.save(new User(user.getUsername(), password, user.getEmail()));
        System.out.println("User gespeichert: " + user.getUsername());
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getAllUser(){

        return (List<User>) userRepository.findAll();

    }
    public UserDTO getUserByEmail(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return new UserDTO(user.getId(), user.getUsername(), user.getEmail());
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, path = "login")
    public AuthorizationDTO login(@RequestBody LoginDTO loginDTO){
        System.out.println("asdfasfd");
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginDTO.getEmail(), loginDTO.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        if (authentication != null && authentication.isAuthenticated()) {
            UserDTO userDTO = getUserByEmail(loginDTO.getEmail());
            String jwtToken = tokenService.generateToken(userDTO);
            System.out.println("yes");
            System.out.println(userDTO);
            return new AuthorizationDTO(jwtToken, userDTO);
        } else {
            throw new AuthenticationServiceException("Authentication is null or user is not authenticated for some reason");
        }
    }

}