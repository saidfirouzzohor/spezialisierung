package SpringProjekt.MainSpring.entity;

import com.sun.istack.NotNull;
import org.springframework.lang.NonNull;
import javax.persistence.*;

@Entity
@Table(name = "User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(unique = true)
    @NotNull
    private String username;
    @Column
    @NotNull
    private String password;
    @Column(unique = true)
    @NotNull
    private String email;

    public User(String userName, String password, String email) {
        this.username = userName;
        this.password = password;
        this.email = email;
    }

    public User() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
