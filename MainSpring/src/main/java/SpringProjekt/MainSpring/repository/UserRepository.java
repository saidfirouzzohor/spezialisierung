package SpringProjekt.MainSpring.repository;

import SpringProjekt.MainSpring.entity.User;
import com.sun.istack.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(@NotNull String email);

}
