import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

axios.interceptors.request.use(config =>{
  const jwt = localStorage.getItem('jwt');
  if(jwt){
    config.headers.Authorization = jwt;
  }
  return config;
})

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userList: [],
    currentUser: null

  },
  getters: {


  },
  mutations: {
    setUser(state, userList){
      state.userList = userList
    },
    setLogin(state, user){
      state.currentUser = user

  }},
  actions:{
    async registerUser(context, user){
      const response = await axios.post('/api/user', user);
      context.commit('setUser', response.data)
    },
    async loginUser(context, user){
      console.log('asdasfasf');
      const response = await axios.post('/api/user/login', user);
      console.log(response.data);
      context.commit('setLogin', response.data.user)
      localStorage.setItem('jwt', response.data.jwt);
    },
    logoutUser(context){
      localStorage.removeItem('jwt');
      context.commit('setLogin', null);
      console.log(localStorage.getItem('jwt'));
      console.log("ich war dirn");
    }


    //mit der unteren Funktion kannst du dir die userdaten aus dem user DTO ins frontend holen
    //in der jeweiligen vue brauchst du noch ein computed und created lifecycleHook damit du in der jeweiligen vue
    //in die vue lädst und du dann die daten dort verarbeiten kannst. (ähnloich wie bei registerUSer ind er Registrieungs.vue) MELDE dich bei mir wenn du dann dort bist
    //,
    // async loadUser(context, user){
    //   const response = await axios.get('api/user', user);
    //   context.commit('setUser', response.date)
    // }

  },
  modules:{

  }
})
