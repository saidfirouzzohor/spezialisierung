import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/LoginSeite.vue'
import Registrierung from '../views/RegistrierungSeite.vue'
import WebShop from '../views/WebShop.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Login //() => import(/* webpackChunkName: "about" */ '../views/LoginSeite.vue')
  },
  {
    path: '/registrierung',
    name: 'Registrierung',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component:  Registrierung//() => import(/* webpackChunkName: "about" */ '../views/RegistrierungSeite.vue')
  },
  {
    path: '/webShop',
    name: 'WebShop',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component:  WebShop//() => import(/* webpackChunkName: "about" */ '../views/RegistrierungSeite.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
